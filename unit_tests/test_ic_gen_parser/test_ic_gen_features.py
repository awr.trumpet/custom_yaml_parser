import pytest
from displaylink.yaml_parser.icgen_yaml_parser import ICGenYamlParser

class TestIcgenFeaturesIndividually:
    @pytest.mark.parametrize('type', ['register', 'descriptor', 'address_map'])
    def test_setting_type_of_icgen_is_correct(self, type):
        some_yaml = f'''
        {type}:
            something1
            something2
        '''
        new_icgen = ICGenYamlParser()
        new_icgen.parse(some_yaml, False)
        assert new_icgen.type == type
