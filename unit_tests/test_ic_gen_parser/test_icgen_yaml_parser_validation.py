import pytest
import os
import yaml
import glob
from displaylink.yaml_parser.icgen_yaml_parser import ICGenYamlParser, outer_schema, includes_schema, views_schema, \
    config_schema, variables_schema, constants_schema
from schema import SchemaError, SchemaWrongKeyError, SchemaUnexpectedTypeError


def invalid_types(valid_types: list):
    type_dictionary = {
        dict: '{key_one: value_one}',
        list: '''
        - item one
        - item two''',
        str: 'some_random_string',
        int: '1',
        float: '2.5',
        bool: 'True'
    }
    return [yaml_example for type_, yaml_example in type_dictionary.items() if type_ not in valid_types]


class TestOutermostDictionaryValidation:
    # @pytest.mark.parametrize('filepath', glob.glob(os.path.join(os.path.dirname(__file__),
    # 'example_definition_files' , '**', '*.yml')))
    def test_all_reference_files_pass_validation(self):
        path_to_file = os.path.join(os.path.dirname(__file__), 'example_definition_files', 'descriptors',
                                    'constants_desc.yml')
        new_icgen_parser = ICGenYamlParser()
        # parsed_yaml = new_icgen_parser.parse(filepath, True)
        parsed_yaml = new_icgen_parser.parse(path_to_file, True)


class TestValidationOnParsing:
    def test_parsing_invalid_outer_yaml_raises_an_assertion_error(self):
        new_icgen_parser = ICGenYamlParser()
        bad_yaml = '''
        This is not a valid key
        1
        2.0
        '''
        with pytest.raises(AssertionError):
            new_icgen_parser.parse(bad_yaml, True)

    def test_parsing_valid_data_structure_without_validation_does_not_raise_an_error(self):
        new_icgen_parser = ICGenYamlParser()
        bad_yaml = '''
        dict_key_one:
            value1
        dict_key_two:
            value2'''
        assert new_icgen_parser.parse(bad_yaml, False)


class TestViewsSectionValidation:
    @pytest.mark.parametrize('invalid_type', invalid_types([dict]))
    def test_non_dictionary_value_raises_an_error(self, invalid_type):
        bad_views_section = f'{invalid_type}'
        with pytest.raises(SchemaError):
            views_schema.validate(bad_views_section)

    def test_views_section_has_only_valid_keys(self):
        bad_views_section = f'''
        cheader: {__file__}
        not_defined_view: {__file__}
        '''
        parsed_yaml = yaml.load(bad_views_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaWrongKeyError):
            views_schema.validate(parsed_yaml)

    def test_includes_section_has_to_have_a_valid_path(self):
        bad_views_section = '''
        cheader: some_undefined_path
        design: some_other_undefined_path
        '''
        parsed_yaml = yaml.load(bad_views_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            views_schema.validate(parsed_yaml)

    def test_all_valid_views(self):
        file_path = __file__
        good_views_section = f'''
        design: {file_path}
        verification: {file_path}
        cheader: {file_path}
        html: {file_path}
        '''
        parsed_yaml = yaml.load(good_views_section, Loader=yaml.SafeLoader)
        assert views_schema.validate(parsed_yaml)


class TestVariablesSectionValidation:
    @pytest.mark.parametrize('invalid_type', invalid_types([dict]))
    def test_non_dictionary_item_raises_an_error(self, invalid_type):
        bad_variables_section = f'{invalid_type}'
        parsed_yaml = yaml.load(bad_variables_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            variables_schema.validate(parsed_yaml)

    def test_non_string_key_raises_error(self):
        bad_variables_section = '''
        - 1: some_value
        '''
        parsed_yaml = yaml.load(bad_variables_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            variables_schema.validate(parsed_yaml)

    @pytest.mark.parametrize("invalid_value", invalid_types([str, int, float]))
    def test_non_integer_or_string_value_raises_error(self, invalid_value):
        bad_variables_section = f'''
        - key_one: {invalid_value}
        '''
        parsed_yaml = yaml.load(bad_variables_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            variables_schema.validate(parsed_yaml)

    def test_any_string_integer_or_float_value_can_be_in_the_constants(self):
        good_variables_section = '''
            - key_one: value_one
            - key_two: 2
            - key_three: 3.0'''
        parsed_yaml = yaml.load(good_variables_section, Loader=yaml.SafeLoader)
        assert variables_schema.validate(parsed_yaml)


class TestConstantsSectionValidation:
    @pytest.mark.parametrize('invalid_type', invalid_types([dict]))
    def test_non_dictionary_item_raises_an_error(self, invalid_type):
        bad_constants_section = f'{invalid_type}'
        parsed_yaml = yaml.load(bad_constants_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            constants_schema.validate(parsed_yaml)

    def test_non_string_key_raises_error(self):
        bad_constants_section = '''
        - 1: some_value
        '''
        parsed_yaml = yaml.load(bad_constants_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            constants_schema.validate(parsed_yaml)

    @pytest.mark.parametrize("invalid_value", invalid_types([int, str, float]))
    def test_non_integer_or_string_value_raises_error(self, invalid_value):
        bad_constants_section = f'''
        - key_one: {invalid_value}
        '''
        parsed_yaml = yaml.load(bad_constants_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            constants_schema.validate(parsed_yaml)

    def test_any_string_integer_or_float_value_can_be_in_the_constants(self):
        good_constants_section = '''
        - key_one: value_one
        - key_two: 2
        - key_three: 3.0
        '''
        parsed_yaml = yaml.load(good_constants_section, Loader=yaml.SafeLoader)
        assert constants_schema.validate(parsed_yaml)


class TestIncludesSectionValidation:
    def test_valid_path_is_validated(self):
        valid_yaml_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'some_random_yaml.yml')
        good_includes_section = f'''
        - {valid_yaml_path}
        - {__file__}
        '''
        parsed_yaml = yaml.load(good_includes_section, Loader=yaml.SafeLoader)
        assert includes_schema.validate(parsed_yaml)

    def test_path_to_a_directory_raises_an_error(self):
        bad_includes_section = f'''
        - {os.path.dirname(__file__)}
        '''
        parsed_yaml = yaml.load(bad_includes_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaError):
            includes_schema.validate(parsed_yaml)

    @pytest.mark.parametrize('invalid_type', invalid_types([list]))
    def test_if_includes_section_is_not_a_list_an_error_is_raised(self, invalid_type):
        bad_includes_section = f'{invalid_type}'
        parsed_yaml = yaml.load(bad_includes_section, Loader=yaml.SafeLoader)
        with pytest.raises(SchemaUnexpectedTypeError):
            includes_schema.validate(parsed_yaml)
