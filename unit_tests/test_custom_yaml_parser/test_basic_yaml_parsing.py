import os
import yaml
import pytest
from schema import Schema
from displaylink.yaml_parser.custom_yaml_parser import CustomYamlParser, ValidationError


class TestYamlLoadingStringInput:

    sample_good_yaml = '''
    name:
        - first: Adam,
          last: Richardson
    date: Today
    age: 22
    '''

    sample_bad_yaml = '''
    This:
    is some -
    bad: yaml
    '''

    def test_loading_good_yaml_from_string(self):
        new_parser = CustomYamlParser()
        assert new_parser.parse(self.sample_good_yaml) == yaml.load(self.sample_good_yaml, Loader=yaml.SafeLoader)

    def test_loading_incorrect_yaml_string_raises_an_error(self):
        new_parser = CustomYamlParser()
        with pytest.raises(yaml.YAMLError):
            new_parser.parse(self.sample_bad_yaml)

    def test_trying_to_validate_with_no_schema_raises_an_error(self):
        new_parser = CustomYamlParser()
        with pytest.raises(ValidationError):
            new_parser.validate(None, yaml.load(self.sample_good_yaml, Loader=yaml.SafeLoader))


    def test_trying_to_validate_with_no_data_raises_an_error(self):
        random_schema = Schema([])
        new_parser = CustomYamlParser()
        with pytest.raises(ValidationError):
            new_parser.validate(random_schema, None)


class TestYamlFromFile:

    def test_loading_yaml_from_file(self):
        test_yaml_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'some_random_yaml.yml')
        new_parser = CustomYamlParser()
        with open(test_yaml_file) as yaml_file:
            assert yaml.load(yaml_file, Loader=yaml.SafeLoader) == new_parser.parse(test_yaml_file)

    def test_loading_yaml_from_directory_of_yaml_files_raises_an_error(self):
        directory_string = os.path.dirname(__file__)
        new_parser = CustomYamlParser()
        with pytest.raises(NotImplementedError):
            new_parser.parse(directory_string)

    def test_loading_yaml_file_with_bad_syntax_raises_an_error(self):
        bad_yaml_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'some_bad_yaml.yml')
        new_parser = CustomYamlParser()
        with pytest.raises(yaml.YAMLError):
            new_parser.parse(bad_yaml_file)
