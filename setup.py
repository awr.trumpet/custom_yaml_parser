from setuptools import setup, find_packages

MODULE_NAME = 'displaylink-yamllib'

PYTHON_DIR = '.'

setup(
    name=MODULE_NAME,
    author='DisplayLink (UK) Ltd',
    author_email='asicbuilduser@displaylink.com',
    description='Library of custom yaml parsers used by DisplayLink',
    url='',
    license='Proprietary',
    classifiers=[
        'Programming Language :: Python :: 3.7',
        'License :: OTHER/PROPRIETARY LICENSE',
        'Operating System :: POSIX',
        'Private :: Do Not Upload to the public pypi server',
    ],
    install_requires=[
        'PyYAML>=3.0',
        'schema'
    ],
    extras_require={
        'test': [
            'pytest>=4.0.0',
            'pytest-timeout',
        ],
    },
    # use_scm_version={'root': '..', 'relative_to':__file__},
    packages=find_packages(PYTHON_DIR, exclude=['*.tests']),
    entry_points={
        'console_scripts': [
        ],
    }
)
