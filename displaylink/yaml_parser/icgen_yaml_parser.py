from os.path import isfile
from displaylink.yaml_parser.custom_yaml_parser import CustomYamlParser
from schema import Schema, Or, And, Optional

views_schema = Schema({Optional('cheader'): And(str, lambda s: isfile(s)),
                       Optional('design'): And(str, lambda s: isfile(s)),
                       Optional('verification'): And(str, lambda s: isfile(s)),
                       Optional('html'): And(str, lambda s: isfile(s))})

config_schema = Schema({Optional('library_name'): str,
                        Optional('max_bank_address'): And(int, lambda i: i >= 0),
                        Optional('prefix'): str,
                        Optional('version'): int,
                        Optional('name'): str,
                        Optional('!repetition'):
                            {'number': And(int, lambda i: i > 0),
                             'offset': And(int, lambda i: i > 0)},
                        Optional('bus_width'): And(int, lambda i: i % 32),
                        Optional('word_width'): And(int, lambda i: i % 32),
                        Optional('address_width'): And(int, lambda i: i % 32)})

constants_schema = Schema([{str: Or(str, int, float)}])

variables_schema = Schema([{str: Or(str, int, float)}])

includes_schema = Schema([And(str, isfile)])

register_field_schema = Schema([{'name': str,
                                 'range': [int, int],
                                 'description': str,
                                 'rw': Or('RO', 'RC', 'RS', 'RW', 'WO', 'WOC', 'WOS', 'W1C', 'W1S', 'W1T', 'W0C', 'W0S',
                                          'W0T', only_one=True),
                                 'strobe': Or('single', 'byte', 'both', 'none', only_one=True),
                                 'stall': Or('True', 'False', only_one=True),
                                 'access': Or('internal', 'external', only_one=True),
                                 Optional('Bins'): {str: [int]}}])

descriptor_field_schema = Schema([{'name': str,
                                   'description': str,
                                   'range': [int, int]}])

address_map_field_schema = Schema([{'name': str,
                                    'description': str,
                                    'range': [int, int]}])

component_schema = Schema([{'name': str,
                            'address': int,
                            'description': str,
                            'fields': Or(register_field_schema, descriptor_field_schema, address_map_field_schema,
                                         only_one=True),
                            Optional('sw_guide'): str,
                            Optional('group'): str,
                            Optional('!repetition'): {'number': And(int, lambda i: i > 0),
                                                      'offset': And(int, lambda i: i > 0)}}])

outer_schema = Schema({Or('registers', 'address_map', 'descriptor', only_one=True): component_schema,
                       Optional('views'): views_schema,
                       Optional('constants'): constants_schema,
                       Optional('variables'): variables_schema,
                       Optional('config'): config_schema,
                       Optional('includes'): includes_schema})


class ICGenYamlParser(CustomYamlParser):
    def __init__(self):
        super().__init__()
        self.type = None

    def parse(self, input_, validate=True) -> dict:
        parsed_yaml = super(ICGenYamlParser, self).parse(input_)
        assert isinstance(parsed_yaml, dict), 'YAML file does not produce a dictionary when parsed.'
        if validate:
            parsed_yaml = outer_schema.validate(parsed_yaml)
        self._deduce_type(parsed_yaml.keys())
        return parsed_yaml

    def _deduce_type(self, yaml_keys_list) -> None:
        if 'register' in yaml_keys_list:
            self.type = 'register'
        elif 'address_map' in yaml_keys_list:
            self.type = 'address_map'
        elif 'descriptor' in yaml_keys_list:
            self.type = 'descriptor'
