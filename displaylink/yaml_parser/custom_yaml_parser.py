import yaml
import os
from schema import Schema


class ValidationError(Exception):
    pass


class CustomYamlParser:

    def __init__(self):
        pass

    @staticmethod
    def validate(schema: Schema, data: dict):
        if schema is None:
            raise ValidationError(
                'No schema provided when validating, please provide a schema when calling the validate method.')
        elif data is None:
            raise ValidationError(
                'No data provided to validate against, please make sure you specify data when calling the validate '
                'method.')
        else:
            return schema.validate(data)

    def parse(self, input_: str) -> dict:
        if os.path.exists(input_):
            path_input = os.path.abspath(input_)
            if os.path.isdir(path_input):
                raise NotImplementedError
            elif os.path.isfile(path_input):
                with open(path_input) as yaml_file:
                    return yaml.load(yaml_file, Loader=yaml.SafeLoader)
        else:
            return yaml.load(input_, Loader=yaml.SafeLoader)

    @staticmethod
    def dump(yaml_dictionary, output_file=None) -> None:
        if output_file:
            with open(output_file, 'w') as yaml_file:
                yaml_file.write(yaml.dump(yaml_dictionary, yaml_file))
        else:
            print(yaml.dump(yaml_dictionary))
